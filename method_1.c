#include<stdio.h> //ст. заголовочный файл
#include<stdlib.h>  // ст биб-ла содерджит ф-ии, занимающиеся выделением памяти и т.д
#include<math.h>// мат. биб-ла
#define eps 0.00001 // точность вычисления
double func(double,double,double); // тип ф-ии func
double firstmethod(double,double,double,double,double); // прототип выч. функции , первый метод 
double func(double x,double c,double d) 
{
	return pow(x, 4) + c * pow(x, 3) - d * x; // Это полином, который мы возвращаем
}
double firstmethod(double a,double b,double x,double c, double d) //ф-ия , которая вычисляет корень
{
	double m; // объявляем переменные 
	double lenght = b-a; // объявляем и выч. длину оьтрезка
	if(fabs(func(a,c,d))<eps) // если абсолютное значение ф-ии с переменными a,c,d меньше точности , то ....
	return a;	//	printf("koren a\n");
	else if(fabs(func(b,c,d))<eps) // если абсолютное значение ф-ии с переменными b,c,d меньше точности , то ....
	return b; //printf("koren b\n");
	while(lenght>=eps){
	m = (a+b)/2;// считаем середину отрезка
	if(fabs(func(m,c,d))<eps) // если абсолютное значение ф-ии с переменными m,c,d меньше точности , то ....
	  return m;	 //printf("koren c\n");
	if(func(a,c,d) * func(m,c,d) < 0)
		b = m;
	else
		a = m;
	lenght/=2;// сокращаем длину
    }
    return m; //возвращаем 
}
int main()
{
	double a,b,c,d,x;
	printf("Введите длину a и b: "); scanf("%lf%lf",&a,&b); // вводим длину итервала
	printf("Введите c и d: "); scanf("%lf%lf",&c,&d); //вводим пар-ры
	if(func(a,c,d)*func(b,c,d)>0) // устанавливаем проверку
		printf("Ф-я не имеет корней на данном отрезке\n");
	x = firstmethod(a,b,x,c,d); //считаем
	printf("x = %lf, f(x) = %lf\n",x,func(x,c,d)); //выводим
	return 0;
}
 