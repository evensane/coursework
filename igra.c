#include<stdio.h>
#include<stdlib.h>
#include<string.h>

#define COMMAND_SIZE 30
#define ADD 30
#define SUB 31
#define GOTO 40
#define LOAD 20
#define DIV 32
#define INPUT 10
#define OUTPUT 11
#define STORE 21 
#define NEG_GOTO 41
#define ZERO_GOTO 42
#define HALT 43
#define COMMAND_LENGTH 100

//прототипы
void shell();
void computer(char *);
void sign_in();
void display();
char button();
void output_menu();
void output_menu1();
int joy_menu(int *);
void intrpr(char*,int*, int*);
int alu(int,int,int*,int*,FILE **);
int menu();
int menu1();
int i=0, j=0,i_start=0,i_end=2,i_menu=0; // global
int mass[3][3]={0};
int alu(int command,int argument,int *acc, int *ram,FILE **prog) // получает команду и аргумент
{
	 switch(command){
	   case ADD:
	     *acc += ram[argument];
		 break;
		case GOTO:
		  fseek(*prog,argument,SEEK_SET);
		  break;
		case LOAD:
		  *acc = ram[argument];
		  break;
	    case SUB:
	     *acc -= ram[argument];
		 break;
		case DIV:
	     *acc /= ram[argument];
		 break;
		case INPUT:
		  scanf("%d", &ram[argument]);
		  break;
	    case OUTPUT:
		  printf("%d\n",*acc);
		  break;
		case STORE:
		  ram[argument] = *acc;
		  break;
		case NEG_GOTO:
		  if(*acc < 0)
			fseek(*prog,argument,SEEK_SET);
		  break;
		case ZERO_GOTO:
		  if(*acc == 0)
			fseek(*prog,argument,SEEK_SET);
		  break;
		case HALT:
		  return 1;
		  break;
     }
	 return 0;
}

void intrpr(char *line,int *command, int *argument) //получает команду и аргумент из потока ввода и преобразовывает буквенные комманды в числовые
{
	char comm[COMMAND_SIZE];
	char arg[COMMAND_SIZE];
	int i;
	for(i = 0; line[i] != ' ' && line[i] != '\n';i++)
      comm[i] = line[i];
    comm[i] = '\0';
    if(!strcmp(comm,"ADD"))
      *command = ADD;
    else if(!strcmp(comm,"SUB"))
		*command = SUB;
	else if(!strcmp(comm,"GOTO"))
		*command = GOTO;
	else if(!strcmp(comm,"LOAD"))
		*command = LOAD;
	else if(!strcmp(comm,"DIV"))
		*command = DIV;
	else if(!strcmp(comm,"INPUT"))
		*command = INPUT;
	else if(!strcmp(comm,"OUTPUT"))
		*command = OUTPUT;
	else if(!strcmp(comm,"STORE"))
		*command = STORE;
	else if(!strcmp(comm,"NEG_GOTO"))
		*command = NEG_GOTO;
	else if(!strcmp(comm,"ZERO_GOTO"))
		*command = ZERO_GOTO;
	else if(!strcmp(comm,"HALT"))
		*command = HALT;
	else
		*command = atoi(comm);
	for(; i != EOF && i != '\n';i++)
		arg[i] = line[i];
	*argument = atoi(arg);
}
char button()  //разрешает необрабатываемый ввод и вывод
{
    char select;
        system("stty raw"); 
        select = getchar();
        system("stty cooked");
    return select;
}
void display() //очищает дисплей каждый раз при вызове
{
		system("clear");
}
int joy_menu(int *flag) // с помощью switch и ascii кода преобразовали в функцию, которая позволяет выбирать в меню нужный варинант.
{
	char select;
	select = button();
	switch(select)
	{
		case 13:
		{
			display();
			*flag = 0;
			return i_menu;
		}
		case 56: // вверх
		{
			if (i_menu == i_start) {
				i_menu = i_end;
				//output_menu(i);
				display();
				return i_menu;
			} 
			if (i_menu > i_start) {
				i_menu--;
				//output_menu(i);
				display();
				return i_menu;
			}
		}
		case 50: // вниз
		{
			if (i_menu == i_end) {
				i_menu = i_start;
				//output_menu(i);
				display();
				return i_menu;
			} else {
				i_menu++;
				//output_menu(i);
				display();
				return i_menu;
			}
		}
		default: 
		{
			display();
		}
	}
}
void computer(char *filename) // считывает значение из файла с операциями и передает аргументы в след ф-ю, где на основе выбора вып. операции
{ 
	 int *ram;
	 char line[COMMAND_SIZE];
	 int command;
	 int argument;
	 int result;
	 ram=(int*)calloc(100,sizeof(int));
	 int acc = 0;
	 FILE *prog;
	  if((prog = fopen(filename,"r")) == NULL){
	 printf("do not open this file");
	 }
	 printf("Вывод: \n");
	 while(fgets(line,COMMAND_SIZE,prog) != NULL){
		 
		 intrpr(line,&command,&argument);
		 result = alu(command,argument,&acc,ram,&prog);
		 if(result)
			break;
	 }
	 
}
void shell() // ф-ия реализации shell для нашего компьютера
{
  char command[COMMAND_LENGTH];
  printf("Welcome to new shell!\n");
  printf("shell :> ");
    while(fgets(command,COMMAND_LENGTH,stdin) != NULL){
      char *h=strchr(command,'\n');
      *h = '\0';
	command[COMMAND_LENGTH-1] = '\0';
	   if(!strncmp(command,"run ",4))
    {
	computer(strchr(command,' ')+1);
	continue;
	}
      system(command);
      printf("shell :> ");
     }
}


int menu() //меню с выбором функций
{
	int flag = 1;
	i = 0, j = 0;
	i_menu = 0;
	for (int i = 0; i < i_end + 1; i++) {
		for (int j = 0; j < i_end + 1; j++) {
			mass[i][j] = 0;
		}
	}
	while (flag == 1)
	{
		
		output_menu(i_menu);
		joy_menu(&flag);
	}
	if (i_menu == 0) 
		sign_in();
	if (i_menu == 1) 
		shell();
	if (i_menu == 2)
		exit(1);
}
int menu1()
{
	int flag = 1;
	i = 0, j = 0;
	i_menu = 0;
	for (int i = 0; i < i_end + 1; i++) {
		for (int j = 0; j < i_end + 1; j++) {
			mass[i][j] = 0;
		}
	}
	while (flag == 1)
	{
		
		output_menu1(i_menu);
		joy_menu(&flag);
	}
	if (i_menu == 0) 
		shell();
	if (i_menu == 2)
		exit(1);
}
void output_menu(int kursor)
{
  if(kursor == 0)
    printf("\t\t[*] - Авторизоваться\n\t\t - shell\n\t\t - Выход\n");
  if(kursor == 1)
    printf("\t\t - Авторизоваться\n\t\t[*] - shell\n\t\t - Выход\n");
  if(kursor == 2)
    printf("\t\t - Авторизоваться\n\t\t - shell\n\t\t[*] - Выход\n");
}
void output_menu1(int kursor)
{ 
   printf("\t\n");
  if(kursor == 0)
    printf("\t\t[*] - shell\n\t\t - Выход\n");
  if(kursor == 2)
    printf("\t\t -  shell\n\t\t [*]- Выход\n");

}
void sign_in() //ф-я позволяющая имитировать авторизацию на компьютере
{
char user[100] = "admin", pass[100]="admin", log_pass[100];
FILE *auth = fopen("auth.txt", "a+");
while(!feof(auth)) fscanf(auth, "%s\n", pass);
fclose(auth);
int flag = 1;
printf("%s\n", pass);
	while (flag)
	{
	display();
	printf("\t\tЗдравствуйте, %s\n\t\tВведите свой пароль и для подтверждения нажмите [ENTER]: \n", user);
	scanf("%s", log_pass);
	if (strcmp(log_pass, pass) == 0) {
	printf("Вы успешно авторизовались!\n");
	flag = 0;
		}
	}
}
int main()
{
	printf("Доброго времени суток и приятного пользования!\n\n");
	printf("Чтобы использовать арифметические/логические операции пиши в shell - run name(sum.txt,div.txt,sub.txt), а так же можно создать файл самому\n");
	printf("Чтобы авторизоваться используйте стандартный пароль - admin,возможно позже будет возможность редактирования пользователя \t\n");
	printf("Чтобы выбирать в меню используйте кнопки '3'- вверх и '2'- вниз, 'Enter'- выбрать \t\t\n");
menu();
    printf("Добро пожаловать в систему Admin!\n\n");
menu1();
return 0;
}