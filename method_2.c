#include<stdio.h>//ст. заголовочный файл
#include<stdlib.h>// ст биб-ла содерджит ф-ии, занимающиеся выделением памяти и т.д
#include<math.h>// мат. биб-ла
#define eps 0.00001 //точность
#define delta 0.00001 //берем маленькое значение , для касательной
double func(double,double,double); // прот. ф-ии func
double secondmethod(double,double,double); //прот. ф-ии secondmethod
double func(double x,double c,double d) // ф-я полинома
{
	return pow(x,4)+ c * pow(x,3) - d*x;  // возвращаем полином
}
double dirivitive(double x,double c,double d) //ф-я нахождения производной
{
	return (func(x+delta,c,d)-func(x-delta,c,d))/((x+delta)-(x-delta)); //возвращаем производную от полинома
}
double secondmethod(double x,double c,double d) //вычисляем корень с помощью этой ф-ии
{
	double x1,x2;
	int count = 0; //пермеменная для итнерациий
	do{
		count++; //инкримент счестчик
	x1 = x - (func(x,c,d))/(dirivitive(x,c,d)); // формула подсчета корня из методички, кстати в ней опечатка и производная находится в знаменателе.
	x2 = x; // уменьшеаем интервал
	x = x1; 
	}
	while(fabs(x1-x2)>=eps); //  пока не достигнута точность eps(0.0000001)
	printf("count = %d\n",count); // выводим интераций количетсво
	return x1; //возвращаем корень
}
int main()
{
	double x,c,d; //объявляем перменные
	printf("Начальное значение x: "); scanf("%lf",&x); //ввод нач значения икса
	printf("Введите c и d: "); scanf("%lf%lf",&c,&d); //ввод пар-ов
	x = secondmethod(x,c,d); // считаем
	printf("x = %lf, f(x) = %lf\n",x,func(x,c,d)); //выводим находимую точку корень
	return 0;
}